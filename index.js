let Metalsmith = require('metalsmith');
let markdown = require('metalsmith-markdown-remarkable');
let permalinks = require('metalsmith-permalinks');
let ancestry = require('metalsmith-ancestry');
let drafts = require('metalsmith-drafts');
let layouts = require('metalsmith-layouts');
let assets = require('metalsmith-assets');
let sass = require('metalsmith-sass');
let watch = require('metalsmith-watch');
let msIf = require('metalsmith-if');
let wikilinks = require('metalsmith-wikilinks');
let breadcrumbs = require('metalsmith-breadcrumbs');

let production = (process.env.MODE != 'develop');
let oneshot = (process.env.ONCE == '1');
let siteurl = (production ? 'https://cadet.life' : 'http://localhost:2015/cadet')

let ms = Metalsmith(__dirname);

ms
.metadata({
	sitename: "Cadet",
	siteurl: siteurl,
	generatorname: "Metalsmith",
	generatorurl: "http://metalsmith.io",
	productionBuild: production
})
.source('./content')
.destination('./public')
.clean(true)
.use(
	msIf(!production && !oneshot,
		watch({
			paths: {
				"${source}/**/*": true,
				"templates/**/*": "**/*.md",
				"partials/**/*": "**/*.md"
			},
			livereload: (!production && !oneshot)
		})
	)
)
.use(sass({
	outputDir: "assets/css/",
	outputStyle: "compressed",
	functions: {
		'siteurl($path: "")': function(path){
			let sass = require('node-sass');
			return new sass.types.String(siteurl + path.getValue());
		}
	}
}))
.use(drafts())
.use(ancestry())
.use(breadcrumbs())
.use(wikilinks())
.use(markdown({
	html: true,
	linkify: false
}))
.use(permalinks({
	relative: false
}))
// .use(writemetadata({
	// pattern: ['**/*'],
	// bufferencoding: 'utf8'
// }))
.use(layouts({
	engine: "mustache",
	directory: "templates",
	partials: "partials",
	partialsExtension: ".mustache"
}))
.use(assets({
	source: "assets",
	destination: "assets"
}))
.build(function(err){
	if(err) throw err;
});

var extname = require('path').extname;
var dirname = require('path').dirname;

module.exports = plugin;

function plugin(opts){
	return function(files, metalsmith, done){
		Object.keys(files).forEach(function(file){
			if(extname(file) === ".md"){
				let md = files[file].contents.toString();
				md = md.replace(/\[(.*?)\]\(->(.*?)\)/g, function(match, text, link){
					link = link.replace(/^\s*(.*)\s*$/, '$1');
					link = link.toLowerCase().replace(/\s/, '-');
					if(link.indexOf("/") == -1){
						link = dirname(file) + "/" + link;
					}
					return "[" + text + "](" + metalsmith.metadata().siteurl + "/" + link + ")";
				});
				files[file].contents = new Buffer(md);
			}
		});
		setImmediate(done);
	}
}
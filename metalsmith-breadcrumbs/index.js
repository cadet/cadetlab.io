var extname = require('path').extname;
var dirname = require('path').dirname;

module.exports = plugin;

function plugin(opts){
	return function(files, metalsmith, done){
		Object.keys(files).forEach(function(file){
			let paths = file.split("/").slice(0, -1);
			files[file].breadcrumbs = [];
			let path = "";
			paths.forEach(function(p){
				if(path != ""){ path += "/"; }
				path += p;
				if(files[path + "/index.md"] && file != (path + "/index.md")){
					files[file].breadcrumbs.push(files[path + "/index.md"]);
				}
			});
		});
		setImmediate(done);
	}
}
---
title: Home
layout: default.mustache
---
# Welcome, space cadet!

Cadet is a space-exploration-themed website for [Jonathan Stoler](https://jonstoler.me)'s experiments in AI, lifestyle, language, quantification, and design.

## Current Projects

[**Log**](-> wiki/software/log): productivity logger  
[**Denu**](-> wiki/denu): date and time system for space explorers  
[**Mu**](-> wiki/mu): Cadet's official programming language

## Upcoming Projects

* Log GUI
* Habit tracker
* Budget software
* Sleep tracker
* Alarm clock
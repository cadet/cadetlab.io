---
title: Log
layout: default.mustache
description: time tracker
logo: log_logo.svg
---

![Log Logo](-> assets/log_logo.svg)
<style>
	img[alt="Log Logo"]{ width: 150px; height: 150px; }
</style>

# Log

**Log** is a creativity logger.

<p><a class="button" target="_blank" href="https://gitlab.com/cadet/log">Fork Me on GitLab</a></p>

---

`log s [Realm] [Project]`: start tracking

`log e`: end tracking

`log v [Event]`: track event

`log c`: get current realm and project

`log d`: dump logged history
---
title: Kiko
layout: default.mustache
---

> **Warning!** This page is a work in progress. Information may be incomplete or change drastically.

**Kiko** is the official conlang of Cadet. Its syntax is mainly inspired by [Toki Pona](http://tokipona.org/) and [Lietal](http://wiki.xxiivv.com/Lietal). Its phonology is inspired by [Toki Pona](http://tokipona.org/) and Japanese. Its orthography is inspired by Hangul and [Elianscript](http://www.ccelian.com/concepca.html). Its dictionary is inspired by [Toki Pona](http://tokipona.org/) and the [ULD](http://uld3.org/).

## Goals
- Small language, easy to learn.
- Syntatically unambiguous - write a computer program to translate sentences literally.
- Allow for creativity and expression in both spoken and written Kiko.

## Phonology

kmnpst aeiou 

Phonemes are CVN (consonant + vowel + optional "n").

## Grammar

Kiko uses SOV word order.

If the subject is "self," it can be left out.

### Word Construction

Words are made up of three components: aspect (optional), core, and tags (optional).

**Aspect** changes the context of the word without altering its meaning. **Aspect** is used, for example, to modify tense, plurality, confidence, etc.

**Core** is the word itself. Words have no parts of speech. Parts of speech are determined by the word order of the sentence.

**Tags** alter the meaning of a word by attaching other words. This adds specificity to Kiko's broad vocabulary while retaining simple sentence structure.

To say a sentence like "You are reading this article" in Kiko, you would use the following translation:

- "You": stays the same.
- "are reading": Aspect("current", "ongoing") + Core("learn") + Tag("book")
- "this article": Core("creation") + Tag("words", "written", "specific")

## Romanization

Aspects are written with an ampersat ("@") and tags are written with a pipe ("|"). Letters are given their English equivalents.

you @current @ongoing learn|book creation|words|written|specific.

Yes this breaks twitter. I'm working on it.
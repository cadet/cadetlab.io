---
title: Denu
layout: default.mustache
logo: denu_logo.svg
---

![Denu Logo](-> assets/denu_logo.svg)
<style>
	img[alt="Denu Logo"]{ width: 150px; height: 150px; }
</style>

<div>
<blockquote class="plain">It is currently <span id="sama"></span> <span id="dota"></span>.</blockquote>

<script type="text/javascript">
function update(){
	var now = new Date();
	var dota = Math.round((now.getUTCSeconds() + (now.getUTCMinutes() * 60) + (now.getUTCHours() * 60 * 60)) / 86.4);
	document.getElementById("dota").innerText = "@" + dota;
	var pentaNames = ["mipakuto", "mimupa", "miteno", "misenpa", "mitoko", "mipame", "minantu", "misumina", "minana", "mipinpino", "mikutan", "mipuda", "mimase"];
	var monthLen = [31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	var year = now.getUTCFullYear();
	monthLen[1] = (year % 4 != 0 ? 28 : (year % 100 != 0 ? 29 : (year % 400 != 0 ? 28 : 29)));
	var dayOfYear = 0;
	for(var i = 0; i < now.getUTCMonth(); i++){
		dayOfYear += monthLen[i];
	}
	dayOfYear += now.getUTCDate();
	if(dayOfYear == 365){
		document.getElementById("sama").innerText = year + " Year Day";
	} else if(dayOfYear == 366){
		document.getElementById("sama").innerText = year + " Leap Day";
	} else {
		var penta = Math.ceil(dayOfYear / 28) - 1;
		var sama = (dayOfYear % 28 == 0) ? 28 : (dayOfYear % 28);
		document.getElementById("sama").innerText = [year, pentaNames[penta], sama].join(" ");
	}
	now = new Date();
	setTimeout(update, 86400 - (now.getTime() % 86400));
}
update();
</script>

</div>

**Denu** is a date and time system for space explorers.

## Units

| Unit | Name |
| :--- | :--- |
| denu | year |
| penta | month |
| niko | week |
| sama | day |
| nami | hour |
| dota | minute |

## Time

**Denu** uses a version of [Swatch Internet Time](https://en.wikipedia.org/wiki/Swatch_Internet_Time).

Every day (**sama**) is divided into 10 hours (**nami**), each comprised of 100 minutes (**dota**). The day starts at midnight UTC.

Time is formatted as `@ + [total dota]`. The time of day starts at @0 and ends at @999.

When precision is needed, **dota** can be divided into thousands.

## Dates

**Denu** uses a version of the [International Fixed Calendar](https://en.wikipedia.org/wiki/International_Fixed_Calendar) for dates.

Dates are made up of years, months, weeks, and days.

Years (**denu**) are the same as Earth years. There are 365 days in a typical year, and 366 in a leap year.

Every month (**penta**) is exactly 28 days long. There are 13 months in a year.

Weeks (**niko**) are 7 days long. Weeks start on Monday and end on Sunday. Similarly, every year and month starts on a Monday and ends on a Sunday.

Every year also has a **Year Day** at the end of the year. Year Days do not belong to any week or month and are treated as a weekend. Leap years have a **Leap Day** after the Year Day. Leap Days also do not belong to any week or month and are treated as a weekend.

Dates have two formatting methods: full and brief. Full dates are formatted as `[denu] [penta] [sama]`. Brief dates are formatted as `[denu].[penta as integer, zero-padded].[sama]`.

## Conversion

Each **dota** is 86.4 seconds.  
Each **nami** is 100 **dota** (86.4 minutes).  
Each **sama** is 10 **nami** or 1000 **dota** (24 hours).  
Each **niko** is 7 **sama** (7 days).  
Each **penta** is 28 **niko** (28 days).  
Each **denu** is 13 **penta** and 1 **sama** (365 days). Leap years have an additional **sama** to make 366 days.

| Penta Name | Meaning | Corresponding Gregorian Range (Non-Leap Year) |
| :--------- | :------ | :---------------------------- |
| mipakuto | birth | January 1 - January 28 |
| mimupa | move | January 29 - February 25 |
| miteno | learn | February 26 - March 25 |
| misenpa | grow | March 26 - April 22 |
| mitoko | act | April 23 - May 20 |
| mipame | change | May 21 - June 17 |
| minantu | exile | June 18 - July 15 |
| misumina | discover | July 16 - August 12 |
| minana | combine | August 13 - September 9 |
| mipinpino | pause | September 10 - October 7 |
| mikutan | pain | October 8 - November 4 |
| mipuda | breath | November 5 - December 2 |
| mimase | death | December 3 - December 30 |
| Year Day<br>(not part of a month) | | December 31 |
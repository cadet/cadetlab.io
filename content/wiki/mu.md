---
title: Mu
layout: default.mustache
logo: mu_logo.svg
---

> **Warning!** This page is a work in progress. Information may be incomplete or change drastically.

![Mu Logo](-> assets/mu_logo.svg)
<style>
	img[alt="Mu Logo"]{ width: 150px; height: 150px; }
</style>

**Mu** is Cadet's official programming language. It is essentially an implementation of [Io](http://iolanguage.org/), but with a smaller standard library, better documentation, and simpler embedding.

The current version of **Mu** is written in [Crystal](https://crystal-lang.org) but eventually I want to port it to [Rust](https://www.rust-lang.org).

Eventually, I hope to run this website using **Mu**.

## Syntax

Everything is an Object. Everything.

```
Dog := Object clone
Dog bark := method("arf!" println)

fido := Dog clone
fido bark // => "arf!"
```